<?php

namespace Database\Factories;

use App\Enums\Role;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;

/**
 * @extends Factory<User>
 */
class UserFactory extends Factory
{
    public static ?string $password;

    public function definition(): array
    {
        return [
            'role' => Role::admin,
            'email' => fake()->unique()->safeEmail,
            'phone' => fake()->unique()->phoneNumber,
            'password' => static::$password ??= Hash::make('password'),
            'verified_at' => now(),
        ];
    }
}
