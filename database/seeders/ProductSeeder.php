<?php

namespace Database\Seeders;

use App\Models\AttrKey;
use App\Models\AttrValue;
use App\Models\AttrValueSelection;
use App\Models\Category;
use App\Models\Content;
use App\Models\Image;
use App\Models\Product;
use App\Models\Selection;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class ProductSeeder extends Seeder
{
    public function run(): void
    {
        if (!Product::exists()) {
            $data = json_decode(file_get_contents(storage_path('fake/json/products.json')), true);

            foreach ($data as $value) {
                $this->create($value);
            }
        }
    }

    protected function create(array $data, ?int $category_id = null): void
    {
        $category = $this->category($data, $category_id);

        if (isset($data['children'])) {
            foreach ($data['children'] as $child) {
                $this->create($child, $category->id);
            }
        }

        if (isset($data['products'])) {
            foreach ($data['products'] as $product) {
                $this->product($product, $category->id);
            }
        }
    }

    protected function category(array $data, ?int $category_id = null): Category
    {
        $categoryData = [
            'name' => $data['name'],
            'slug' => Str::slug($data['name']['en']),
        ];

        if ($category_id) {
            $categoryData['category_id'] = $category_id;
        }

        $category = Category::firstOrCreate($categoryData, array_merge($categoryData, [
            'description' => $data['description'],
        ]));

        if (!$category->images) {
            $category->images()->save(new Image(['values' => fakeImages('category')]));
        }

        return $category;
    }

    protected function product(array $data, int $category_id): void
    {
        $slug = Str::slug($data['name']['en']);

        $product = Product::createOrFirst([
            'slug' => $slug,
        ], [
            'category_id' => $category_id,
            'slug' => $slug,
        ]);

        if (!$product->images) {
            $product->images()->save(new Image(['values' => fakeImages('product')]));
        }

        foreach (config('app.locales') as $locale => $locale_name) {
            if (!$product->{'content_' . $locale}) {
                $product->{'content_' . $locale}()->save(new Content([
                    'locale' => $locale,
                    'name' => $data['name'][$locale],
                    'description' => $data['description'][$locale],
                ]));
            }
        }

        foreach ($data['attributes'] as $attributes) {
            foreach ($attributes as $attribute => $value) {
                $attrKey = AttrKey::whereSlug($attribute)->first();

                if ($attrKey) {
                    if (!$attrKey->translatable) {
                        $value = [
                            'default' => $value,
                        ];
                    }

                    AttrValue::firstOrCreate([
                        'attr_key_id' => $attrKey->id,
                        'product_id' => $product->id,
                    ], [
                        'attr_key_id' => $attrKey->id,
                        'product_id' => $product->id,
                        'value' => $value,
                    ]);
                }
            }
        }

        if (!$product->selections->count()) {
            foreach ($data['selections'] as $selection) {
                $this->selection($selection, $product);
            }
        }
    }

    protected function selection(array $data, Product $product): void
    {
        $selection = Selection::create([
            'product_id' => $product->id,
            'quantity' => rand(5, 10),
            'price' => rand(200, 300),
        ]);

        $selection->images()->save(new Image(['values' => fakeImages('selection')]));

        foreach ($data as $attributes) {
            foreach ($attributes as $attribute => $value) {
                $attrKey = AttrKey::whereSlug($attribute)->first();

                if ($attrKey) {
                    if (!$attrKey->translatable) {
                        $value = [
                            'default' => $value,
                        ];
                    }

                    AttrValueSelection::firstOrCreate([
                        'attr_key_id' => $attrKey->id,
                        'product_id' => $product->id,
                        'selection_id' => $selection->id,
                    ], [
                        'attr_key_id' => $attrKey->id,
                        'selection_id' => $selection->id,
                        'value' => $value,
                    ]);
                }
            }
        }
    }
}
