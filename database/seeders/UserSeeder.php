<?php

namespace Database\Seeders;

use App\Enums\Role;
use App\Models\User;
use Database\Factories\UserFactory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    public function run(): void
    {
        UserFactory::$password = Hash::make('password');

        if (!User::where('email', 'admin@admin.com')->exists()) {
            User::factory()->create([
                'email' => 'admin@admin.com',
            ]);
        }

        for ($i = 1; $i <= 15; $i++) {
            User::factory()->create([
                'role' => Role::seller,
            ]);
        }

        for ($i = 1; $i <= 30; $i++) {
            User::factory()->create([
                'role' => Role::customer,
            ]);
        }
    }
}
