<?php

namespace Database\Seeders;

use App\Models\AttrKey;
use Illuminate\Database\Seeder;

class ProductAttributeSeeder extends Seeder
{
    protected array $attributes_type1 = [
        'weight',
        'ingredients',
        'calories',
        'proteins',
        'fats',
        'carbohydrates',
        'volume',
        'nutritional_and_energy_value',
        'size',
        'country_of_production',
        'neckline',
        'height_type',
        'insulation',
        'collection',
        'care',
        'lining_material',
        'color',
        'ram',
        'storage',
        'display',
        'memory',
        'camera',
        'height',
        'capacity',
        'temperature_zones',
        'max_load',
        'power',
        'pump_pressure',
        'suction_power',
        'bowl_capacity',
    ];

    protected array $attributes_type2 = [
        'size_on_model',
        'processor',
    ];

    public function run(): void
    {
        foreach ($this->attributes_type1 as $value) {
            $this->createAttribute($value);
        }

        foreach ($this->attributes_type2 as $value) {
            $this->createAttribute($value, false);
        }
    }

    protected function createAttribute(string $value, bool $translatable = true): void
    {
        $name = [];

        foreach (config('app.locales') as $locale => $locale_name) {
            $name[$locale] = __("products.attributes.$value", locale: $locale);
        }

        AttrKey::firstOrCreate([
            'slug' => $value,
        ], [
            'slug' => $value,
            'name' => $name,
            'translatable' => $translatable,
        ]);
    }
}
