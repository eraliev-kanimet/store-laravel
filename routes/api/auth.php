<?php

use App\Http\Controllers\Api\Auth\AuthController;
use Illuminate\Support\Facades\Route;

Route::controller(AuthController::class)->group(function () {
    Route::get('show', 'show')->name('show');
    Route::get('logout', 'logout')->name('logout');
});
