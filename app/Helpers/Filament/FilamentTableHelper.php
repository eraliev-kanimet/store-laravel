<?php

namespace App\Helpers\Filament;

use Filament\Tables\Actions\Action;
use Filament\Tables\Actions\CreateAction;
use Filament\Tables\Actions\DeleteAction;
use Filament\Tables\Actions\DeleteBulkAction;
use Filament\Tables\Actions\EditAction;
use Filament\Tables\Columns\IconColumn;
use Filament\Tables\Columns\TextColumn;

class FilamentTableHelper
{
    public function text(string $column, string $label): TextColumn
    {
        return TextColumn::make($column)
            ->label($label);
    }

    public function icon(string $column, string $label): IconColumn
    {
        return IconColumn::make($column)
            ->label($label);
    }

    public function action(string $id): Action
    {
        return Action::make($id);
    }

    public function createAction(): CreateAction
    {
        return CreateAction::make();
    }

    public function editAction(): EditAction
    {
        return EditAction::make();
    }

    public function deleteAction(): DeleteAction
    {
        return DeleteAction::make();
    }

    public function deleteBulkAction(): DeleteBulkAction
    {
        return DeleteBulkAction::make();
    }
}
