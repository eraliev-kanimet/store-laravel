<?php

use Illuminate\Support\Facades\File;

function images(?array $images = null): array
{
    if (is_array($images)) {
        return array_map(fn($image) => asset('storage/' . $image), $images);
    }

    return [];
}

function fakeImage(string $model): string
{
    $dir = storage_path("app/public/$model");

    if (!is_dir($dir)) {
        mkdir($dir, 0777, true);
    }

    $image = rand(1, 30) . '.jpg';

    if (!file_exists("$dir/$image")) {
        File::copy(storage_path("fake/images/$image"), "$dir/$image");
    }

    return "$model/$image";
}

function fakeImages(string $model): array
{
    $images = [];

    for ($i = 0; $i < rand(1, 3); $i++) {
        $images[] = fakeImage($model);
    }

    return $images;
}
