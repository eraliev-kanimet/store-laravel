<?php

require_once 'Filament/functions.php';
require_once 'images.php';

function truncateStr($string, $maxLength = 13)
{
    if (mb_strlen($string) > $maxLength) {
        $string = mb_substr($string, 0, $maxLength) . '...';
    }

    return $string;
}

function filterAvailableLocales(array $locales)
{
    return array_intersect_key(config('app.locales'), array_flip($locales));
}

function cleanAndUniqueWords(string $str = ''): array
{
    if ($str == '') {
        return [];
    }

    return array_unique(array_map('trim', explode(' ', $str)));
}

function convertToInt($id): int
{
    return (int)$id;
}

function sanitizeArrayKeys(array $array): array
{
    $sanitizedArray = [];

    foreach ($array as $key => $value) {
        $sanitizedKey = filter_var($key, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);

        $sanitizedArray[convertToInt($sanitizedKey)] = $value;
    }

    return $sanitizedArray;
}
