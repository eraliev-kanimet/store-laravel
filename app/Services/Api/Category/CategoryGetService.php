<?php

namespace App\Services\Api\Category;

use App\Http\Resources\CategoryResource;
use App\Models\Category;
use App\Services\Abstract\PaginateModelWithCacheService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class CategoryGetService extends PaginateModelWithCacheService
{
    protected string $tag = 'api_categories';

    protected function query(): Builder
    {
        return Category::query()
            ->with(['categories.categories.categories', 'images'])
            ->whereCategoryId(null);
    }

    protected function resource(mixed $result): AnonymousResourceCollection
    {
        CategoryResource::$locale = config('app.locale');

        return CategoryResource::collection($result);
    }
}
