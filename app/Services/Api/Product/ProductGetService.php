<?php

namespace App\Services\Api\Product;

use App\Http\Resources\ProductResource;
use App\Models\Category;
use App\Models\Product;
use App\Services\Abstract\PaginateModelWithCacheService;
use App\Services\Models\AttrKey\AttrKeyService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ProductGetService extends PaginateModelWithCacheService
{
    protected string $tag = 'api_products';
    protected string $locale;

    protected int $category_id = 0;

    protected array $words = [];
    protected array $categories = [];
    protected array $attributes = [];

    protected array $integers = ['page', 'per_page', 'category_id'];

    public function __construct(array $data)
    {
        parent::__construct($data);

        $this->locale = config('app.locale');

        $this->words = cleanAndUniqueWords($data['q'] ?? '');

        $this->attributes = $data['attributes'] ?? [];
    }

    protected function query(): Builder
    {
        $with = [
            'category.category.category',
            'images',
            'attr.attrKey',
            'selections' => [
                'attr.attrKey',
                'images',
            ],
            'content_' . $this->locale,
        ];

        $query = Product::query()
            ->with($with)
            ->whereIsAvailable(true)
            ->whereHas('selections', function (Builder $query) {
                $query->where('is_available', true);
            })
            ->whereHas('content_' . $this->locale, function (Builder $query) {
                if (count($this->words)) {
                    foreach ($this->words as $word) {
                        $query->whereAny(
                            ['name', 'description'], 'like', "%$word%"
                        );
                    }
                }
            });

        if ($this->category_id) {
            $query->whereIn('category_id', $this->categories());
        }

        if (count($this->attributes)) {
            $this->sanitizeAttributes();

            foreach ($this->attributes as $attribute => $value) {
                $query->whereHas('attr', function (Builder $query) use ($attribute, $value) {
                    $query->where('attr_key_id', $attribute)->whereIn($value['key'], $value['value']);
                });
            }
        }

        return $query
            ->orderByDesc('sorted')
            ->orderByDesc('category_id');
    }

    protected function resource(mixed $result): AnonymousResourceCollection
    {
        ProductResource::$locale = $this->locale;

        return ProductResource::collection($result);
    }

    protected function generateCacheKey(): string
    {
        $attributes = $this->attributes;

        ksort($attributes);

        $attributesKey = md5(json_encode($attributes));

        return sprintf(
            $this->tag . '_%s_%s_%s_%s_%s_%s',
            $this->locale,
            $this->page,
            $this->per_page,
            '(' . implode(',', $this->words) . ')',
            '(' . implode(',', $this->categories) . ')',
            $attributesKey,
        );
    }

    protected function categories(): array
    {
        if ($this->category_id) {
            $category = Category::with('categories.categories.categories')->find($this->category_id);

            if ($category) {
                return $category->categoryIds();
            }

            throw new NotFoundHttpException('Category was not found!');
        }

        return $this->categories;
    }

    protected function sanitizeAttributes(): void
    {
        $service = AttrKeyService::x();

        $attributes = sanitizeArrayKeys($this->attributes);

        $arr = [];

        foreach ($service->all()->whereIn('id', array_keys($attributes))->all() as $attrKey) {
            $arr[$attrKey->id] = [
                'key' => $attrKey->translatable ? "value->$this->locale" : 'value->default',
                'value' => array_map('trim', explode('@', $attributes[$attrKey->id]))
            ];
        }

        $this->attributes = $arr;
    }
}
