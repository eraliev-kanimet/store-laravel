<?php

namespace App\Services\Api\User;

use App\Http\Resources\UserResource;
use App\Models\User;
use App\Services\Abstract\Service;
use Illuminate\Support\Facades\Auth;

class UserService extends Service
{
    public function __construct(
        readonly public User $record
    )
    {}

    public static function attempt(array $data): static|false
    {
        $data['is_active'] = true;

        if (Auth::attempt($data)) {
            return new static(Auth::user());
        }

        return false;
    }

    public function login(): void
    {
        Auth::login($this->record);
    }

    public function logout(): void
    {
        $this->record->tokens()->delete();

        request()->session()->invalidate();
        request()->session()->regenerateToken();
    }

    public function getResource(): UserResource
    {
        return new UserResource($this->record);
    }

    public function getResourceWithToken(): array
    {
        return [
            'data' => $this->getResource(),
            'token' => $this->record->createToken(config('app.name'))->plainTextToken,
        ];
    }
}
