<?php

namespace App\Services\Models\Product;

use App\Models\Content;
use App\Models\Image;
use App\Models\Product;
use App\Services\Abstract\Service;

class ProductModifyService extends Service
{
    public function __construct(
        readonly public Product $product
    )
    {}

    public function contentCreate(string $name, string $description, string $locale): void
    {
        $this->product->{"content_$locale"}()->save(new Content(compact(['name', 'description', 'locale'])));
    }

    public function contentUpdate(string $name, string $description, string $locale): void
    {
        $content = [];

        if ($this->product->{"content_$locale"}->name != $name) {
            $content['name'] = $name;
        }

        if ($this->product->{"content_$locale"}->description != $description) {
            $content['description'] = $description;
        }

        if (count($content)) {
            $this->product->{"content_$locale"}->update($content);
        }
    }

    public function contentsCreate(array $data, array $locales): void
    {
        foreach ($locales as $locale) {
            $this->contentCreate(
                $data['name'][$locale],
                $data['description'][$locale],
                $locale
            );
        }
    }

    public function contentsUpdate(array $data, array $locales): void
    {
        foreach ($locales as $locale) {
            if ($this->product->{"content_$locale"}) {
                $this->contentUpdate(
                    $data['name'][$locale],
                    $data['description'][$locale],
                    $locale
                );
            } else {
                $this->contentCreate(
                    $data['name'][$locale],
                    $data['description'][$locale],
                    $locale
                );
            }
        }
    }

    public function imagesCreate(array $images): void
    {
        $this->product->images()->save(new Image(['values' => $images]));
    }

    public function imagesUpdate(array $images): void
    {
        $this->product->images()->update([
            'values' => $images
        ]);
    }
}
