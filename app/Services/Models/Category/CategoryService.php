<?php

namespace App\Services\Models\Category;

use App\Models\Category;
use App\Services\Abstract\Service;
use Illuminate\Support\Facades\Cache;

class CategoryService extends Service
{
    protected int $ttl = 604800;

    public function __construct(
        readonly protected string $locale
    )
    {}

    public function cacheOptions(): array
    {
        return Cache::remember('category_options_' . $this->locale, $this->ttl, fn() => $this->options());
    }

    public function options(): array
    {
        return Category::get(['name', 'id'])->pluck('name.' . $this->locale, 'id')->toArray();
    }
}
