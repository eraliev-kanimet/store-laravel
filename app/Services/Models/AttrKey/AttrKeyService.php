<?php

namespace App\Services\Models\AttrKey;

use App\Models\AttrKey;
use App\Services\Abstract\Service;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;

class AttrKeyService extends Service
{
    public static ?Collection $attr_keys = null;

    protected int $ttl = 604800;

    public function __construct(
        readonly protected ?string $locale = null
    )
    {}

    public function cacheOptions(): array
    {
        $key = 'attr_key_options_' . $this->locale;

        return Cache::remember($key, $this->ttl, fn() => $this->options());
    }

    public function options(): array
    {
        $attributes = [];

        foreach ($this->all() as $item) {
            $attributes[$item->id] = $item->name[$this->locale];
        }

        return $attributes;
    }

    public function all(): Collection
    {
        if (is_null(self::$attr_keys)) {
            self::$attr_keys = Cache::remember('attr_keys', $this->ttl, fn() => AttrKey::all());
        }

        return self::$attr_keys;
    }
}
