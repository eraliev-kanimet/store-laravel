<?php

namespace App\Enums;

enum Role: int
{
    case admin = 1;
    case seller = 2;
    case courier = 3;
    case moderator = 4;
    case customer = 5;

    public static function options(): array
    {
        $array = [];

        foreach (self::cases() as $case) {
            $array[$case->value] = $case->translate();
        }

        return $array;
    }

    public function translate(): string
    {
        return __('common.roles.' . $this->name);
    }
}
