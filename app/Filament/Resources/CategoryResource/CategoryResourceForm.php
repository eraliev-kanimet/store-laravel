<?php

namespace App\Filament\Resources\CategoryResource;

use App\Helpers\Filament\FilamentFormHelper;
use Filament\Forms\Form;
use Illuminate\Support\Collection;

class CategoryResourceForm
{
    public static function form(
        Form             $form,
        array|Collection $categories,
        ?int             $category_id,
        bool             $create = true
    ): Form
    {
        $helper = new FilamentFormHelper;

        $categorySelect = $helper
            ->select('category_id')
            ->options($categories)
            ->label(__('common.category'))
            ->hidden(is_null($category_id));

        if ($create) {
            $categorySelect->default($category_id)->disabled(!is_null($category_id));
        } else {
            $categorySelect->nullable(is_null($category_id));
        }

        $locales = array_keys(config('app.locales'));

        return $form->schema([
            $categorySelect,
            $helper->tabsInput('name', $locales, true, __('common.name')),
            $helper->tabsTextarea('description', $locales, false, __('common.description')),
            $helper->image('images')
                ->multiple()
                ->imageEditor()
                ->label(__('common.images')),
        ]);
    }
}
