<?php

namespace App\Filament\Resources;

use App\Filament\Resources\AttrKeyResource\Pages;
use App\Helpers\Filament\FilamentFormHelper;
use App\Models\AttrKey;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Illuminate\Support\Facades\Auth;

class AttrKeyResource extends Resource
{
    protected static ?string $model = AttrKey::class;

    protected static ?string $navigationIcon = 'heroicon-o-rectangle-stack';

    public static function getNavigationLabel(): string
    {
        return __('common.attributes_label');
    }

    public static function getPluralLabel(): string
    {
        return __('common.attributes_label');
    }

    public static function form(Form $form): Form
    {
        $helper = new FilamentFormHelper;

        return $form
            ->schema([
                $helper->toggle('translatable')
                    ->label(__('common.translatable'))
                    ->default(true),
                $helper->tabsInput(
                    'name',
                    array_keys(config('app.locales')),
                    true,
                    __('common.name')
                ),
            ])
            ->columns(1);
    }

    public static function canViewAny(): bool
    {
        return Auth::user()->hasRoleAdmin();
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListAttrKeys::route('/'),
            'create' => Pages\CreateAttrKey::route('/create'),
            'edit' => Pages\EditAttrKey::route('/{record}/edit'),
        ];
    }
}
