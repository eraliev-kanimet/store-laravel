<?php

namespace App\Filament\Resources\AttrKeyResource\Pages;

use App\Filament\Resources\AttrKeyResource;
use App\Models\AttrKey;
use Filament\Actions;
use Filament\Resources\Pages\ListRecords;
use Filament\Tables\Table;

class ListAttrKeys extends ListRecords
{
    protected static string $resource = AttrKeyResource::class;

    public function getTitle(): string
    {
        return __('common.attributes_label');
    }

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }

    public function table(Table $table): Table
    {
        $helper = filamentTableHelper();

        $locale = config('app.locale');

        return $table
            ->columns([
                $helper->text('name', __('common.name'))
                    ->formatStateUsing(fn(AttrKey $record) => $record->name[$locale]),
                $helper->icon('translatable', __('common.translatable'))
                    ->alignCenter()
                    ->boolean(),
            ])
            ->actions([
                $helper->editAction(),
                $helper->deleteAction(),
            ])
            ->bulkActions([
                $helper->deleteBulkAction(),
            ]);
    }
}
