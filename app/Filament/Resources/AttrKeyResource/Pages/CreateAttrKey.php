<?php

namespace App\Filament\Resources\AttrKeyResource\Pages;

use App\Filament\Resources\AttrKeyResource;
use Filament\Resources\Pages\CreateRecord;

class CreateAttrKey extends CreateRecord
{
    protected static string $resource = AttrKeyResource::class;

    public function getTitle(): string
    {
        return __('common.create_attribute');
    }
}
