<?php

namespace App\Filament\Resources\AttrKeyResource\Pages;

use App\Filament\Resources\AttrKeyResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditAttrKey extends EditRecord
{
    protected static string $resource = AttrKeyResource::class;

    public function getTitle(): string
    {
        return __('common.edit_attribute');
    }

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
