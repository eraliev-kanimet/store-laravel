<?php

namespace App\Filament\Resources\UserResource\Pages;

use App\Enums\Role;
use App\Filament\Resources\UserResource;
use App\Models\User;
use Filament\Actions\CreateAction;
use Filament\Resources\Pages\ListRecords;
use Filament\Tables\Table;

class ListUsers extends ListRecords
{
    protected static string $resource = UserResource::class;

    public function getTitle(): string
    {
        return __('common.users');
    }

    public function table(Table $table): Table
    {
        $helper = filamentTableHelper();

        return $table
            ->columns([
                $helper->text('email', __('common.email')),
                $helper->text('phone', __('common.phone_number')),
                $helper->text('role', __('common.role'))
                    ->formatStateUsing(fn(User $user) => Role::from($user->role)->translate()),
                $helper->icon('is_active', __('common.active'))
                    ->alignCenter()
                    ->boolean(),
                $helper->icon('verified_at', __('common.verified'))
                    ->alignCenter()
                    ->boolean(),
            ])
            ->actions([
                $helper->editAction(),
            ]);
    }

    protected function getHeaderActions(): array
    {
        return [
            CreateAction::make()
                ->label(__('common.create_user')),
        ];
    }
}
