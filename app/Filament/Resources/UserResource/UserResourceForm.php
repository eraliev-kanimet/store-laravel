<?php

namespace App\Filament\Resources\UserResource;

use App\Enums\Role;
use Illuminate\Database\Eloquent\Model;

class UserResourceForm
{
    public static function form(): array
    {
        $helper = filamentFormHelper();

        return [
            $helper->toggle('is_active')
                ->label(__('common.active'))
                ->default(true)
                ->columnSpanFull(),
            $helper->input('email')
                ->label(__('common.email'))
                ->required()
                ->email()
                ->unique(ignorable: fn(?Model $record): ?Model => $record),
            $helper->input('phone')
                ->label(__('common.phone_number'))
                ->required()
                ->unique(ignorable: fn(?Model $record): ?Model => $record),
            $helper->input('password')
                ->label(__('common.password'))
                ->required(fn(?Model $record): bool => is_null($record))
                ->password()
                ->maxLength(255),
            $helper->select('role')
                ->label(__('common.role'))
                ->options(Role::options()),
        ];
    }
}
