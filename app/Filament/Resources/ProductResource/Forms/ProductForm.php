<?php

namespace App\Filament\Resources\ProductResource\Forms;

use App\Helpers\Filament\FilamentFormHelper;
use App\Models\AttrKey;
use App\Services\Models\AttrKey\AttrKeyService;
use Filament\Forms\Components\Repeater;
use Filament\Forms\Get;

final class ProductForm
{
    protected FilamentFormHelper $helper;
    protected AttrKeyService $attrKeyService;

    public function __construct(
        protected array $attributes = [],
        readonly array  $categories = [],
        readonly array  $locales = [],
        readonly bool   $category_disabled = false,
        readonly bool   $edit = false
    )
    {
        $this->helper = new FilamentFormHelper;
        $this->attrKeyService = AttrKeyService::x(config('app.locale'));
    }

    public function setAttributes(array $attributes): void
    {
        $this->attributes = $attributes;
    }

    public function form(): array
    {
        $tabs = [
            $this->helper->tab(__('common.basic'), [
                $this->helper->grid([
                    $this->helper->select('category_id')
                        ->label(__('common.category'))
                        ->required()
                        ->disabled($this->category_disabled)
                        ->options($this->categories)
                        ->searchable()
                        ->columnSpan(2),
                    $this->helper->input('sorted')
                        ->label(__('common.sorted'))
                        ->numeric()
                        ->minValue(0)
                        ->maxValue(10000),
                ], 3),
                $this->helper->tabsInput('name', $this->locales, true, __('common.name')),
                $this->helper->tabsTextarea('description', $this->locales, true, __('common.description')),
                $this->helper->grid([
                    $this->helper->toggle('is_available')
                        ->label(__('common.is_available'))
                        ->default(true),
                ])
            ]),
        ];

        if ($this->edit) {
            $tabs[] = $this->helper->tab(__('common.properties'), [$this->attributesRepeater()]);
        }

        $tabs[] = $this->helper->tab(__('common.images'), [
            $this->helper->image('images')
                ->label('')
                ->imageEditor()
                ->multiple()
                ->required()
        ]);

        return [$this->helper->tabs($tabs)];
    }

    public function attributesRepeater(bool $required = true): Repeater
    {
        return $this->helper->repeater('attr', [
            $this->helper->select('attr_key_id')
                ->native()
                ->label(__('common.attribute'))
                ->options($this->attributes)
                ->required()
                ->reactive(),
            $this->helper->grid(fn(Get $get) => $this->attributeValue($get), 1)
                ->hidden(fn(Get $get) => is_null($get('attr_key_id'))),
        ])
            ->relationship('attr')
            ->label('')
            ->required($required)
            ->addActionLabel(__('common.add_property'));
    }

    protected function attributeValue(Get $get): array
    {
        $attr_key_id = $get('attr_key_id');

        if (!$attr_key_id) {
            return [];
        }

        /** @var AttrKey $attrKey */
        $attrKey = $this->attrKeyService->all()->firstWhere('id', $attr_key_id);

        if (!$attrKey) {
            return [];
        }

        $key = 'default';

        if ($attrKey->translatable) {
            if (count($this->locales) == 1) {
                $key = $this->locales[0];
            } else {
                $inputs = [];

                foreach (filterAvailableLocales($this->locales) as $locale => $name) {
                    $inputs[] = $this->helper->input("value.$locale")
                        ->label(__('common.meaning_in') . ' ' . $name)
                        ->required();
                }

                return $inputs;
            }
        }

        return [
            $this->helper->input("value.$key")->label(__('common.value'))->required()
        ];
    }
}
