<?php

namespace App\Filament\Resources\ProductResource\Forms;

use App\Helpers\Filament\FilamentFormHelper;

class SelectionForm
{
    public static function form(array $attributes, array $locales): array
    {
        $helper = new FilamentFormHelper;

        $productForm = new ProductForm(
            $attributes,
            locales: $locales,
        );

        return [
            $helper->tabs([
                $helper->tab(__('common.basic'), [
                    $helper->grid([
                        $helper->input('quantity')
                            ->label(__('common.quantity'))
                            ->minValue(0)
                            ->required()
                            ->numeric(),
                        $helper->input('price')
                            ->label(__('common.price'))
                            ->minValue(0)
                            ->required()
                            ->numeric(),
                    ]),
                    $helper->toggle('is_available')
                        ->label(__('common.is_available'))
                        ->default(true),
                ]),
                $helper->tab(__('common.properties'), [
                    $productForm->attributesRepeater(false)
                        ->defaultItems(0),
                ]),
                $helper->tab(__('common.images'), [
                    $helper->image('images')
                        ->label(__('common.images'))
                        ->multiple()
                        ->imageEditor(),
                ]),
            ]),
        ];
    }
}
