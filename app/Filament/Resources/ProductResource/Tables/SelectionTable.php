<?php

namespace App\Filament\Resources\ProductResource\Tables;

use App\Helpers\Filament\FilamentTableHelper;
use App\Models\Selection;
use Filament\Tables\Actions\CreateAction;

class SelectionTable
{
    public FilamentTableHelper $helper;

    public function __construct()
    {
        $this->helper = new FilamentTableHelper;
    }

    public function columns(): array
    {
        return [
            $this->helper->text('properties', __('common.properties'))
                ->state(function (Selection $selection) {
                    $locale = config('app.locale');

                    $string = '';

                    foreach ($selection->attr->slice(0, 10) as $attribute) {
                        $key = $attribute->attrKey->name[$locale];

                        if ($attribute->attrKey->translatable) {
                            $value = $attribute->value[$locale];
                        } else {
                            $value = $attribute->value['default'];
                        }

                        $string .= $key . ': ' . $value . ', ';
                    }

                    if ($string) {
                        return trim($string, ', ');
                    }

                    return __('common.no_properties');
                }),
            $this->helper->text('price', __('common.price'))
                ->alignCenter(),
            $this->helper->text('quantity', __('common.quantity'))
                ->alignCenter(),
            $this->helper->icon('is_available', __('common.is_available'))
                ->alignCenter()
                ->boolean(),
        ];
    }

    public function actions(): array
    {
        return [
            $this->helper->editAction()
                ->modalWidth('6xl')
                ->mutateRecordDataUsing(function (array $data, Selection $selection) {
                    $data['images'] = $selection->images->values;

                    return $data;
                })
                ->after(function (array $data, Selection $selection) {
                    $selection->images->update(['values' => $data['images']]);
                }),
            $this->helper->deleteAction(),
        ];
    }

    public function headerActions(): array
    {
        return [
            $this->createAction()
        ];
    }

    public function bulkActions(): array
    {
        return [
            $this->helper->deleteBulkAction(),
        ];
    }

    public function emptyStateActions(): array
    {
        return [
            $this->createAction()
        ];
    }

    protected function createAction(): CreateAction
    {
        return $this->helper->createAction()
            ->modalWidth('6xl')
            ->after(function (array $data, Selection $selection) {
                $selection->images()->create(['values' => $data['images']]);
            });
    }
}
