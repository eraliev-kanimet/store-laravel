<?php

namespace App\Filament\Resources\ProductResource\RelationManagers;

use App\Filament\Resources\ProductResource\Forms\SelectionForm;
use App\Filament\Resources\ProductResource\Tables\SelectionTable;
use App\Models\Product;
use App\Services\Models\AttrKey\AttrKeyService;
use Filament\Forms\Form;
use Filament\Resources\RelationManagers\RelationManager;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Model;

class SelectionsRelationManager extends RelationManager
{
    protected static string $relationship = 'selections';

    public Model|Product $ownerRecord;

    public array $attr = [];

    public static function getTitle(Model $ownerRecord, string $pageClass): string
    {
        return __('common.selections');
    }

    public static function getPluralRecordLabel(): string
    {
        return __('common.selections');
    }

    public static function getModelLabel(): ?string
    {
        return __('common.selection');
    }

    public function form(Form $form): Form
    {
        if (!count($this->attr)) {
            $service = AttrKeyService::x(config('app.locale'));

            $this->attr = $service->cacheOptions();
        }

        return $form->schema(
            SelectionForm::form($this->attr, config('app.locales'))
        )->columns(1);
    }

    public function table(Table $table): Table
    {
        $selectionTable = new SelectionTable;

        return $table
            ->columns($selectionTable->columns())
            ->headerActions($selectionTable->headerActions())
            ->actions($selectionTable->actions())
            ->bulkActions($selectionTable->bulkActions())
            ->emptyStateHeading(__('common.selections_not_found.header'))
            ->emptyStateDescription(__('common.selections_not_found.description'))
            ->emptyStateActions($selectionTable->emptyStateActions());
    }
}
