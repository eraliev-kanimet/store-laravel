<?php

namespace App\Filament\Resources\ProductResource\Pages;

use App\Filament\Resources\ProductResource;
use App\Filament\Resources\ProductResource\Forms\ProductForm;
use App\Models\Product;
use App\Services\Models\AttrKey\AttrKeyService;
use App\Services\Models\Category\CategoryService;
use App\Services\Models\Product\ProductModifyService;
use Filament\Actions\DeleteAction;
use Filament\Forms\Form;
use Filament\Resources\Pages\EditRecord;
use Illuminate\Database\Eloquent\Model;

class EditProduct extends EditRecord
{
    protected static string $resource = ProductResource::class;

    public string|int|null|Model|Product $record;

    public array $categories = [];
    public array $attr = [];
    public array $locales = [];

    public bool $category_disabled = false;

    public function mount($record): void
    {
        $this->record = $this->resolveRecord($record);

        $this->locales = array_keys(config('app.locales'));

        $locale = config('app.locale');

        $this->setCategories($locale);
        $this->setAttributes($locale);

        $this->authorizeAccess();

        $this->fillForm();

        $this->previousUrl = url()->previous();
    }

    public function form(Form $form): Form
    {
        $productForm = new ProductForm(
            $this->attr,
            $this->categories,
            $this->locales,
            $this->category_disabled,
            true,
        );

        return parent::form($form)
            ->schema($productForm->form())
            ->columns(1);
    }

    protected function mutateFormDataBeforeFill(array $data): array
    {
        $data['images'] = $this->record->images->values;

        foreach ($this->locales as $locale) {
            if ($this->record->{"content_$locale"}) {
                $data['name'][$locale] = $this->record->{"content_$locale"}->name;
                $data['description'][$locale] = $this->record->{"content_$locale"}->description;
            }
        }

        return $data;
    }

    public function afterSave(): void
    {
        $service = ProductModifyService::x($this->record);

        $service->contentsUpdate($this->data, $this->locales);

        if (isset($this->data['images'])) {
            $service->imagesUpdate($this->data['images']);
        }

        $this->record = $service->product;
    }

    public function getTitle(): string
    {
        return __('common.edit_product');
    }

    protected function getActions(): array
    {
        return [
            DeleteAction::make(),
        ];
    }

    protected function setCategories(string $locale): void
    {
        $this->categories = CategoryService::x($locale)->cacheOptions();

        if (!in_array($this->record->category_id, array_keys($this->categories))) {
            $this->category_disabled = true;

            $this->categories = [
                $this->record->category_id => $this->record->category->name[$locale],
            ];
        }
    }

    protected function setAttributes(string $locale): void
    {
        $this->attr = AttrKeyService::x($locale)->cacheOptions();
    }
}
