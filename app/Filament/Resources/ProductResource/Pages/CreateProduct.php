<?php

namespace App\Filament\Resources\ProductResource\Pages;

use App\Filament\Resources\ProductResource;
use App\Filament\Resources\ProductResource\Forms\ProductForm;
use App\Models\Category;
use App\Models\Product;
use App\Services\Models\Product\ProductModifyService;
use Filament\Forms\Form;
use Filament\Resources\Pages\CreateRecord;
use Illuminate\Database\Eloquent\Model;

class CreateProduct extends CreateRecord
{
    protected static string $resource = ProductResource::class;

    public function getTitle(): string
    {
        return __('common.create_product');
    }

    public null|Model|Product $record;

    public array $categories = [];

    public function mount(): void
    {
        $this->categories = Category::get(['name', 'id'])
            ->pluck('name.' . config('app.locale'), 'id')
            ->toArray();

        parent::mount();
    }

    public function form(Form $form): Form
    {
        $productForm = new ProductForm(
            categories: $this->categories,
            locales: config('app.locales'),
        );

        return parent::form($form)
            ->schema($productForm->form())
            ->columns(1);
    }

    public function create(bool $another = false): void
    {
        $this->authorizeAccess();

        $data = $this->form->getState();

        $this->record = $this->handleRecordCreation($data);

        $service = ProductModifyService::x($this->record);

        $service->imagesCreate($data['images']);
        $service->contentsCreate($data, config('app.locales'));

        $this->getCreatedNotification()?->send();

        $this->redirect($this->getRedirectUrl());
    }

    protected function getFormActions(): array
    {
        $actions = parent::getFormActions();

        unset($actions[1]);

        return $actions;
    }
}
