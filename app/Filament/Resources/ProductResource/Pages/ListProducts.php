<?php

namespace App\Filament\Resources\ProductResource\Pages;

use App\Filament\Resources\ProductResource;
use App\Models\Product;
use Filament\Actions\CreateAction;
use Filament\Resources\Pages\ListRecords;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;

class ListProducts extends ListRecords
{
    protected static string $resource = ProductResource::class;

    protected function getTableQuery(): Builder
    {
        return parent::getTableQuery()->with(['category', 'content_' . config('app.locale')]);
    }

    public function table(Table $table): Table
    {
        $helper = filamentTableHelper();

        $locale = config('app.locale');

        return $table
            ->columns([
                $helper->text('name', __('common.name'))
                    ->state(fn (Product $record) => $record->{"content_$locale"}->name),
                $helper->text("category.name.$locale", __('common.category')),
                $helper->icon('is_available', __('common.is_available'))
                    ->alignCenter()
                    ->boolean(),
                $helper->text('created_at', __('common.created_at'))
                    ->formatStateUsing(fn(Product $product) => $product->created_at->format('Y-m-d')),
                $helper->text('updated_at', __('common.updated_at'))
                    ->formatStateUsing(fn(Product $product) => $product->updated_at->format('Y-m-d')),
            ])
            ->actions([
                $helper->editAction(),
            ])
            ->bulkActions([
                $helper->deleteBulkAction(),
            ]);
    }

    protected function getHeaderActions(): array
    {
        return [
            CreateAction::make()
                ->label(__('common.create_product'))
        ];
    }
}
