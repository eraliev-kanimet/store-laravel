<?php

namespace App\Models;

use App\BaseModels\BaseCategory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;

class Category extends BaseCategory
{
    protected static function boot(): void
    {
        parent::boot();

        self::creating(function (self $category) {
            if (is_null($category->slug)) {
                $category->slug = Str::random(32);
            }
        });

        self::saved(function () {
            self::clearCache();
        });

        self::deleted(function () {
            self::clearCache();
        });
    }

    public function category(): BelongsTo
    {
        return $this->belongsTo(self::class);
    }

    public function categories(): HasMany
    {
        return $this->hasMany(self::class);
    }

    public static function clearCache(): void
    {
        $locales = array_keys(config('app.locales'));
        $keys = [];

        foreach ($locales as $locale) {
            $keys[] = 'category_options_' . $locale;
        }

        foreach ($keys as $key) {
            Cache::forget($key);
        }
    }

    public function categoryIds(): array
    {
        $ids = [$this->id];

        foreach ($this->categories as $category) {
            $ids = array_merge($ids, $category->categoryIds());
        }

        return $ids;
    }
}
