<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Support\Str;

class Product extends Model
{
    protected $fillable = [
        'is_available',
        'category_id',
        'slug',
        'sorted',
    ];

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    public function images(): MorphOne
    {
        return $this->morphOne(Image::class, 'imageable');
    }

    public function content_en(): HasOne
    {
        return $this->hasOne(Content::class)->where('locale', 'en');
    }

    public function content_ru(): HasOne
    {
        return $this->hasOne(Content::class)->where('locale', 'ru');
    }

    public function attr(): HasMany
    {
        return $this->hasMany(AttrValue::class);
    }

    public function selections(): HasMany
    {
        return $this->hasMany(Selection::class);
    }

    protected static function boot(): void
    {
        parent::boot();

        self::creating(function (self $product) {
            if (is_null($product->slug)) {
                $product->slug = Str::random(32);
            }
        });
    }
}
