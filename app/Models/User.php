<?php

namespace App\Models;

use App\BaseModels\BaseUser;
use App\Enums\Role;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class User extends BaseUser
{
    use HasFactory;

    public function hasRoleAdmin(): bool
    {
        return $this->hasRole(Role::admin);
    }

    public function hasRoleSeller(): bool
    {
        return $this->hasRole(Role::seller);
    }
}
