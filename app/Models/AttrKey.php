<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Cache;

class AttrKey extends Model
{
    protected $fillable = [
        'slug',
        'name',
        'translatable',
    ];

    protected function casts(): array
    {
        return [
            'name' => 'array',
        ];
    }

    public function productValues(): HasMany
    {
        return $this->hasMany(AttrValue::class);
    }

    protected static function boot(): void
    {
        parent::boot();

        self::creating(function (self $key) {
            if (is_null($key->slug)) {
                $key->slug = time();
            }
        });

        self::saved(function () {
            self::clearCache();
        });

        self::deleted(function () {
            self::clearCache();
        });
    }

    public $timestamps = false;

    public static function clearCache(): void
    {
        $locales = array_keys(config('app.locales'));

        $keys = [
            'attr_keys',
        ];

        foreach ($locales as $locale) {
            $keys[] = 'attr_key_options_' . $locale;
        }

        foreach ($keys as $key) {
            Cache::forget($key);
        }
    }
}
