<?php

namespace App\Http\Requests\Api\Product;

use App\Http\Requests\Api\PaginateRequest;
use App\Rules\ProductAttrArray;

class IndexRequest extends PaginateRequest
{
    public function rules(): array
    {
        return [
            ...parent::rules(),
            'q' => ['bail', 'nullable', 'string'],
            'category_id' => ['bail', 'nullable', 'integer'],
            'attributes' => ['bail', 'nullable', new ProductAttrArray],
            'attributes.*' => ['bail', 'nullable', 'string'],
        ];
    }
}
