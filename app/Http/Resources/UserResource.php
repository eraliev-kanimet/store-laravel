<?php

namespace App\Http\Resources;

use App\Enums\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * @var User
     */
    public $resource;

    public function toArray(Request $request): array
    {
        return [
            'id' => $this->resource->id,
            'role' => $this->resource->role,
            'role_text' => Role::from($this->resource->role)->translate(),
            'email' => $this->resource->email,
            'phone' => $this->resource->phone,
            'verified_at' => $this->resource->verified_at,
        ];
    }
}
