<?php

namespace App\Http\Resources;

use App\Models\AttrKey;
use Illuminate\Http\Request;

class AttributeResource extends BaseResource
{
    /**
     * @var AttrKey
     */
    public $resource;

    public function toArray(Request $request): array
    {
        return [
            'id' => $this->resource->id,
            'slug' => $this->resource->slug,
            'name' => $this->resource->name[self::$locale],
            'translatable' => (bool)$this->resource->translatable,
        ];
    }
}
