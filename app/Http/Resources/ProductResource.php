<?php

namespace App\Http\Resources;

use App\Models\AttrValue;
use App\Models\AttrValueSelection;
use App\Models\Category;
use App\Models\Content;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class ProductResource extends BaseResource
{
    /**
     * @var Product
     */
    public $resource;

    public function toArray(Request $request): array
    {
        return [
            'id' => $this->resource->id,
            'is_available' => (bool)$this->resource->is_available,
            'category' => $this->resource->category_id ? $this->category($this->resource->category) : null,
            'name' => $this->content(self::$locale)->name,
            'description' => $this->content(self::$locale)->description,
            'images' => images($this->resource->images->values),
            'attributes' => $this->attr($this->resource->attr),
            'selections' => $this->selections(),
        ];
    }

    protected function content(string $locale): Content
    {
        return $this->resource->{"content_$locale"};
    }

    protected function category(Category $category): array
    {
        return [
            'id' => $category->id,
            'name' => $category->name[self::$locale],
            'category' => $category->category_id ? $this->category($category->category) : null,
        ];
    }

    protected function attr(Collection $collection): Collection
    {
        return $collection->map(function ($attr) {
            return [
                'attribute' => $attr->attr_key_id,
                'name' => $attr->attrKey->name[self::$locale],
                'value' => $this->attrValue($attr),
            ];
        });
    }

    protected function attrValue(AttrValue|AttrValueSelection $attr): ?string
    {
        if ($attr->attrKey->translatable) {
            return $attr->value[self::$locale];
        }

        return $attr->value['default'];
    }

    protected function selections(): array
    {
        $array = [];

        foreach ($this->resource->selections as $selection) {
            $array[] = [
                'id' => $selection->id,
                'is_available' => $selection->is_available,
                'quantity' => $selection->quantity,
                'price' => $selection->price,
                'images' => images($selection->images->values),
                'attributes' => $this->attr($selection->attr),
            ];
        }

        return $array;
    }
}
