<?php

namespace App\Http\Resources;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryResource extends BaseResource
{
    /**
     * @var Category
     */
    public $resource;

    public function toArray(Request $request): array
    {
        return [
            'id' => $this->resource->id,
            'category' => $this->resource->category_id ? $this->category($this->resource->category) : null,
            'slug' => $this->resource->slug,
            'name' => $this->resource->name[self::$locale],
            'description' => $this->resource->description[self::$locale],
            'images' => images($this->resource->images->values),
            'children' => $this->children($this->resource),
        ];
    }

    public function category(Category $category): array
    {
        return [
            'id' => $category->id,
            'category' => $category->category_id ? $this->category($category->category) : null,
            'slug' => $category->slug,
            'name' => $category->name[self::$locale],
            'description' => $category->description[self::$locale],
        ];
    }

    public function children(Category $category): array
    {
        $categories = [];

        foreach ($category->categories as $child) {
            $categories[] = [
                'id' => $child->id,
                'slug' => $child->slug,
                'name' => $child->name,
                'description' => $child->description,
                'images' => images($this->resource->images->values),
                'children' => $this->children($child),
            ];
        }

        return $categories;
    }
}
