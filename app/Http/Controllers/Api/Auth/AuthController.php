<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Services\Api\User\UserService;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function show()
    {
        return UserService::x(Auth::user())->getResource();
    }

    public function logout()
    {
        UserService::x(Auth::user())->logout();

        return $this->apiRes();
    }
}
