<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\PaginateRequest;
use App\Http\Resources\CategoryResource;
use App\Models\Category;
use App\Services\Api\Category\CategoryGetService;

class CategoryController extends Controller
{
    public function index(PaginateRequest $request)
    {
        return CategoryGetService::x($request->validated())->get();
    }

    public function show(Category $category)
    {
        CategoryResource::$locale = config('app.locale');

        return new CategoryResource($category);
    }
}
