<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\AttributeResource;
use App\Services\Models\AttrKey\AttrKeyService;

class AttributeController extends Controller
{
    public function index()
    {
        AttributeResource::$locale = config('app.locale');

        return AttributeResource::collection(AttrKeyService::x()->all());
    }
}
