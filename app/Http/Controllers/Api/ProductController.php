<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Product\IndexRequest;
use App\Http\Resources\ProductResource;
use App\Models\Product;
use App\Services\Api\Product\ProductGetService;

class ProductController extends Controller
{
    public function index(IndexRequest $request)
    {
        return ProductGetService::x($request->validated())->get();
    }

    public function show(Product $product)
    {
        if ($product->is_available) {
            return new ProductResource($product);
        }

        return $this->apiRes(status: 404);
    }
}
