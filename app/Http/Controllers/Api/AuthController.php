<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Auth\LoginRequest;
use App\Http\Requests\Api\Auth\RegisterRequest;
use App\Services\Api\User\UserService;
use App\Services\Api\User\UserModifyService;

class AuthController extends Controller
{
    public function register(RegisterRequest $request)
    {
        $service = UserModifyService::create($request->validated());

        $service->login();

        return $this->apiRes($service->getResourceWithToken());
    }

    public function login(LoginRequest $request)
    {
        $service = UserService::attempt($request->validated());

        if ($service) {
            return $this->apiRes($service->getResourceWithToken());
        }

        return $this->apiRes(['message' => __('auth.error_messages.1')], 401);
    }
}
