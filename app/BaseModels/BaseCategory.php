<?php

namespace App\BaseModels;

use App\Models\Image;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphOne;

class BaseCategory extends Model
{
    protected $fillable = [
        'category_id',
        'slug',
        'name',
        'description',
    ];

    protected $casts = [
        'name' => 'array',
        'description' => 'array',
    ];

    public function images(): MorphOne
    {
        return $this->morphOne(Image::class, 'imageable');
    }

    public $timestamps = false;
}
