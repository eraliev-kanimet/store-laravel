<?php

namespace App\BaseModels;

use App\Enums\Role;
use Filament\Panel;
use Filament\Models\Contracts\FilamentUser;
use Filament\Models\Contracts\HasName;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class BaseUser extends Authenticatable implements FilamentUser, HasName
{
    use Notifiable, HasApiTokens;

    protected $table = 'users';

    protected $fillable = [
        'is_active',
        'role',
        'email',
        'phone',
        'password',
        'verified_at',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected function casts(): array
    {
        return [
            'password' => 'hashed',
        ];
    }

    public function getFilamentName(): string
    {
        return $this->email;
    }

    public function hasRole(Role $role): bool
    {
        return $this->role == $role->value;
    }

    public function canAccessPanel(Panel $panel): bool
    {
        return !$this->hasRole(Role::customer);
    }
}
